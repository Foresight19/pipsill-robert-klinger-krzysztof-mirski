
import React from "react";
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import CRoomForm from './CRoomForm';
import Room from './Room';
import Lobby from './Lobby';
import Game from './Game';

export default function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul id="menu">           
            <li><Link to="/new_game">Nowa gra</Link></li>
            <li><Link to="/lobby">Dołącz do gry</Link></li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/game/:id" exact component={Game} >
            <Game />
          </Route>          
          <Route path="/new_game" component={CRoomForm}>
          <CRoomForm />
          </Route>
          <Route path="/lobby" exact component={Lobby}>
            <Lobby />
          </Route>
          <Route path="/lobby/:id" exact component={Room}>
            <Room />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}