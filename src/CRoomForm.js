import React from "react";
import {db} from "./firebase";
import './CRoomForm.css';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';

class CRoomForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          value: ''
        };
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {    this.setState({value: event.target.value});  }
    handleSubmit = (event) => {
      alert('Stworzono pokój o nazwie: ' + this.state.value);
      event.preventDefault();
        
        db.collection('rooms').add({
          room_name: this.state.value,
          players: [],
          inGame: false
        }) .then (doc => {
          console.log(doc.id);
          window.location.replace(`/lobby/${doc.id}`);
          
        });  
        this.setState({
          value: '',
        });
    }
  
    render() {
      return (
        <div id="containerC">
          <div id="westBoxC">
            <form onSubmit={this.handleSubmit}>        
                <p class="marginBottom">Wprowadź nazwe pokoju:</p>
                <p class="marginBottom"><TextField id="filled-basic" label="Pokój" variant="filled" name="room_name" value={this.state.value} onChange={this.handleChange} /></p>   
                <Button type="submit" variant="contained" color="secondary">
                  Załóż Grę
                </Button>
            </form>
          </div>
          <div id="eastBoxC">
            <HomeIcon style={{ fontSize: 343 }}/>
          </div>
        </div>
      );
    }
  }

  export default CRoomForm;