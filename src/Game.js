import React from "react";
import {db} from "./firebase";
import './Game.css';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


class Game extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          room: {},
          player: {},
          answer: "",
          showAnswer: false
        };
    }
  
    componentDidMount() { 
      localStorage.setItem('room_id', window.location.pathname.split('/')[2]);
      this.getRoom(); 
    }

    allReady() {
      if(this.state.room.players.some(player => player.isReady)) {
        this.setState({showAnswer: true})
      }
    }

    getRoom() {
      db.collection('rooms').doc(localStorage.getItem("room_id")).onSnapshot(doc => {
        if(doc.data() != undefined) {
          let obj = doc.data();
          obj.id = doc.id;
          this.setState({room: obj})
          this.setState({player: obj.players.find(player=>player.name==localStorage.getItem("nick"))})
          this.allReady();
        }
      })
    }

    componentWillUnmount() { 
      let players = this.state.room.players;
      players.splice(players.indexOf(players.find(player => player.name == localStorage.getItem('nick'))), 1);
      db.collection('rooms').doc(localStorage.getItem('room_id')).update({ players: players }).then(() => {
        localStorage.removeItem('nick');
        if(players.length === 0) db.collection('rooms').doc(localStorage.getItem("room_id")).delete();
      })     
      localStorage.removeItem('room_id');
     }
     setAnswer = (e) => {
       e.preventDefault();
        this.setState({answer: e.target.value})
     } 
     saveAnswer = (e) => {
       e.preventDefault();
       let players = this.state.room.players; 
       let round = this.state.room.round + 1 ; 
       players.find(player => player.name == localStorage.getItem("nick")).answer = this.state.answer;
       db.collection("rooms").doc(localStorage.getItem('room_id')).update({ players, round}).then (()=>{
        this.setReady();
       });
     }
     setReady = () => {
      let players = this.state.room.players;  
      players.find(player => player.name == localStorage.getItem("nick")).isReady = true;
      db.collection('rooms').doc(localStorage.getItem('room_id')).update({ players }).then(() => {
        console.log('dlaczego')
      });  
    }     
     quest = () => {
       if (!this.state.room.quest_game) return;
       let question = this.state.room.quest_game[0].quest;
       let wynik;
       if (this.state.room.round != 0) {
         wynik = this.showAnswers(); 
           
       }
       else {
         wynik = 
         <>  
          <div id="containerG">
            <h2>RUNDA {this.state.room.round+1}</h2>
            <h3 id="h3G">{question}</h3>       
            <form onSubmit={this.saveAnswer}>
              <p class="marginBottom"><TextField id="filled-basic" label="Odpowiedź" variant="filled" value={this.state.answer} onChange={this.setAnswer} /></p>
              <Button type="submit" variant="contained" color="secondary">
                OK
              </Button>
            </form>
          </div>
          </>
       }
       return (
         <>
         
          {wynik}

         </>
       )
     }
     showAnswers=() => {
          return (
            <>
              <div id="containerG1">
                <ul>{this.state.room.players.map(player => <li>{player.points}pkt, {player.name} : {player.answer}</li>)}</ul>
              </div>
            </>
          )
        
     }

    render() {
      return (
        <>
            
            {this.quest()}
        </>
      );
    }
  }

  export default Game;