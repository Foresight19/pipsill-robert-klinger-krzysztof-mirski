import React from "react";
import {
    Link
  } from "react-router-dom";
import {db} from "./firebase";
import './Lobby.css';

  
  class Lobby extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          rooms: []
        };
        
    }
  
    componentDidMount() { 
      this.getBase();
     }
 
     roomList() {
        const rooms = this.state.rooms;
         
        return (
        <ul id="menuL">{rooms.map((room) =>   room.inGame ? '' : <li><Link to={`/lobby/${room.id}`}>{room.room_name}</Link></li>)}</ul>  );
      }     

    getBase() {       
      db.collection('rooms').onSnapshot(querySnapshot => {
          let rooms = [];
          querySnapshot.forEach(doc => {
            let obj = doc.data();
            obj.id = doc.id;
            rooms.push(obj)
          })
        // console.log(rooms)
        this.setState({
          rooms: rooms
        })
  
        console.log(this.state.rooms)
      }) 
    }
    render() {
      return (  
          <div id="containerL">
            <h1 id="h1L">Dostępne pokoje:</h1>
            {this.roomList()}
          </div>
      );
    }
  }
  
  export default Lobby;
  /* <div>
        {this.state.rooms.map(room => <div>{room}</div>)}
      </div> */