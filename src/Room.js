import React from "react";
import {db} from "./firebase";
import './Room.css';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

class Room extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
            room: {},
            players: [],
            nick: '',
            player: {}
        };
        this.handleChange = this.handleChange.bind(this);
    }
  
    componentDidMount() { 
      localStorage.setItem('room_id', window.location.pathname.split('/')[2]);
      this.getRoom(); 
      
    }

    

    handleChange(event) {    this.setState({nick: event.target.value});  }

    playerList() {
        const players = this.state.players;
        return (  <ul>{players.map((player) =>   <li>{player.name} - {player.isReady ? 'Gotowy' : 'Nie gotowy'}</li>)}</ul>  );        
    }
    
    componentWillUnmount() { 
      let players = this.state.players;
      players.splice(players.indexOf(players.find(player => player.name === localStorage.getItem('nick'))), 1);
      db.collection('rooms').doc(localStorage.getItem('room_id')).update({ players: players }).then(() => {
       
        localStorage.removeItem('nick');
        

        if(players.length === 0) db.collection('rooms').doc(localStorage.getItem("room_id")).delete();
        
        localStorage.removeItem('room_id');
      })
      
      
     }
     startGame() {
       let playersReady = false;
       this.state.room.players.forEach(player => player.isReady ? playersReady = true : true)
       if(playersReady) {
        db.collection('questions').get().then(query => {
            let all_questions = [];
            query.forEach(doc => {
              let obj = doc.data();
              obj.id = doc.id;
              all_questions.push(obj);
          })
            let questions = [];
            for (let i =0; i<1; i++) {
              let index = Math.floor(Math.random()*all_questions.length);
              questions.push(all_questions[index]);
              all_questions.splice(index, 1);
            }
            let players = this.state.players;
            players.forEach(player => player.isReady = false);
            db.collection('rooms').doc(localStorage.getItem('room_id')).update({ quest_game: questions, players: players, inGame: true, round: 0}).then(() => {
              window.location.replace(`/game/${localStorage.getItem('room_id')}`)
              console.log('poka mie to synek')
            });   
        })
      }      
     }
    getRoom() {
      db.collection('rooms').doc(localStorage.getItem("room_id")).onSnapshot(doc => {
        if(doc.data() != undefined) {
          let obj = doc.data();
          obj.id = doc.id;
          this.setState({room: obj})
          this.setState({players: obj.players})
          this.startGame();
        }
        
      })
    }
    
    setNick = (event) => {
      event.preventDefault();
      if(this.state.nick === '') { 
        alert('Prosze podac nick!');
        return;
      }

      let players = this.state.room.players;
      let player = {
        name: this.state.nick, 
        isReady: false,
        answer: "",
        points: 0
      }
      players.push(player);
      this.setState({player});
      

      db.collection('rooms').doc(window.location.pathname.split('/')[2]).update({ players: players }).then(room => {
        
        localStorage.setItem('nick', this.state.nick);
        
        this.setState({nick: ''})
        
      })
      
    }

    setReady = (event) => {
      let players = this.state.players;
      
      players.find(player => player.name == this.state.player.name).isReady = event.target.checked;
      db.collection('rooms').doc(localStorage.getItem('room_id')).update({ players });
      
    }
    isNick() {
      if(localStorage.getItem('nick')) {
        return (
          <>
            <div id="containerRG">
              <h1 class="h13R">Nazwa pokoju: {this.state.room.room_name}</h1>
              <h3 class="h13R">Gracze: {this.playerList()}</h3>

              
              <input type="checkbox" id="ready" placeholder="Gotowy" value={this.state.player.isReady} onChange={this.setReady} />
              <label for="ready">Gotowy?</label>
            </div>
          </>
        )
      } else {
        return (
          <>
          <div id="containerR">
            <form onSubmit={this.setNick}>
              <h1>Podaj nick:</h1>
              <p class="marginBottom"><TextField id="filled-basic" label="Nick" variant="filled" value={this.state.value} onChange={this.handleChange} /></p>
              <Button type="submit" variant="contained" color="secondary">
                Usta nick
              </Button>
            </form>
          </div>
            </>
        )
      }
    }

    render() {
      return (
        <>
        
            {this.isNick()}
            
        </>
      );
    }
  }

  export default Room;